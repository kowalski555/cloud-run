#### Authorization

To ensure smooth and secure interaction with Google Cloud services, it's essential to configure appropriate authorization settings.

##### Deploy Cloud Run Component Operation Permissions

To use the `deploy-cloud-run` component, grant the following roles to your workload identity pool:
- Cloud Storage Admin ([`roles/run.admin`][sa-run-admin]) to get, create and update a service.
- Service Account User ([`roles/iam.serviceAccountUser`][sa-sa-user]) to run operations as the service account.

Run the following command to grant the `roles/run.admin` & `roles/iam.serviceAccountUser`  role to all principals in a workload identity pool matching `developer_access=true` attribute mapping by the gcloud CLI:

``` bash
# Replace ${PROJECT_ID}, ${PROJECT_NUMBER}, ${LOCATION}, ${POOL_ID} with your values below

WORKLOAD_IDENTITY=principalSet://iam.googleapis.com/projects/${PROJECT_NUMBER}/locations/global/workloadIdentityPools/${POOL_ID}/attribute.developer_access/true

gcloud projects add-iam-policy-binding ${PROJECT_ID} --member="${WORKLOAD_IDENTITY}" --role="roles/run.admin"

gcloud projects add-iam-policy-binding ${PROJECT_ID} --member="${WORKLOAD_IDENTITY}" --role="roles/iam.serviceAccountUser"
```

#### Usage

Add the following to your `gitlab-ci.yml`:
``` yaml
include:
  - component: gitlab.com/kowalski555/cloud-run/cloud-run-deploy@<VERSION>
    inputs:
      project_id: "my-project"
      region: "us-central1"
      service: "service-name"
      image: "gcr.io/cloudrun/hello:latest"
      port: "3000"
```

#### Inputs

| Input                   | Description                                                                  | Example                 | Default Value      |
|-------------------------|------------------------------------------------------------------------------|-------------------------|--------------------|
| `project_id`            | (Required) The ID of the Google Cloud project in which to deploy the service | `my-project`            |                    |
| `region`                | (Required) Region in which to deploy the service                             | `europe-west1`          |                    |
| `service`               | (Required) ID of the service or fully-qualified identifier of the service    | `my-service-name`       |                    |
| `image`                 | (Required) Fully-qualified name of the container image to deploy             | `gcr.io/cloudrun/hello` |                    |
| `port`                  | (Required) Port number the service listens on                                | `3000`                  |                    |
| `vpc_connector`         | (Optional) Name of the VPC connector                                         | `my-vpc-connector`      |                    |
| `service_url`           | (Optional) Name of the service to retrieve URL                               | `my-service-back`       |                    |
| `min_instances`         | (Optional) Minimum number of instances                                       | `1`                     |                    |
| `session-affinity`      | (Optional) Enable session affinity                                           | `true`                  |                    |
| `allow_unauthenticated` | (Optional) Allow unauthenticated access to the service                       | `true`                  | `false`            |
| `ingress_internal `     | (Optional)  Set ingress to internal                                          | `true`                  |                    |
| `as`                    | (Optional) The name of the job to execute                                    | `my-job-name`           | `deploy-cloud-run` |
| `stage`                 | (Optional) The GitLab CI/CD stage                                            | `my-stage`              | `deploy`           |

